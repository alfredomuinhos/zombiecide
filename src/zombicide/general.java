package zombicide;

public class general {

	    private String nombre;
	    private int salud;
	    private int saludMaxima;
	    private boolean vivo;

	    // Constructor
	    public void Humanoide(String nombre, int saludMaxima) {
	        this.nombre = nombre;
	        this.saludMaxima = saludMaxima;
	        this.salud = saludMaxima; // Al inicio, la salud es la máxima
	        this.vivo = true; // Al inicio, el humanoide está vivo
	    }

	    // Métodos para acceder y modificar los atributos
	    public String getNombre() {
	        return nombre;
	    }

	    public int getSalud() {
	        return salud;
	    }

	    public int getSaludMaxima() {
	        return saludMaxima;
	    }

	    public boolean isVivo() {
	        return vivo;
	    }

	    // Método para recibir daño
	    public void recibirDanio(int danio) {
	        salud -= danio;
	        if (salud <= 0) {
	            salud = 0;
	            vivo = false;
	            System.out.println(nombre + " ha sido derrotado.");
	        }
	    }

	    // Método para curarse
	    public void curarse(int cantidad) {
	        if (vivo) {
	            salud += cantidad;
	            if (salud > saludMaxima) {
	                salud = saludMaxima;
	            }
	            System.out.println(nombre + " se ha curado. Salud actual: " + salud);
	        } else {
	            System.out.println(nombre + " no puede curarse porque está muerto.");
	        }
	    }

	    // Método para revivir (usado en caso de necesidad, por ejemplo, en el caso de jugadores)
	    public void revivir() {
	        if (!vivo) {
	            vivo = true;
	            salud = saludMaxima; // Restauramos la salud máxima al revivir
	            System.out.println(nombre + " ha sido revivido con salud completa.");
	        } else {
	            System.out.println(nombre + " ya está vivo.");
	        }
	    }

	    // Método toString para representación textual del objeto
	    @Override
	    public String toString() {
	        return "Humanoide{" +
	                "nombre='" + nombre + '\'' +
	                ", salud=" + salud +
	                ", saludMaxima=" + saludMaxima +
	                ", vivo=" + vivo +
	                '}';
	    }
	 // Clase Zombie (clase padre)
	    public class Zombie {
	        protected int movimiento;
	        protected int danio;
	        protected String tipo;

	        // Constructor
	        public Zombie(int movimiento, int danio, String tipo) {
	            this.movimiento = movimiento;
	            this.danio = danio;
	            this.tipo = tipo;
	        }

	        // Métodos para acceder y modificar los atributos
	        public int getMovimiento() {
	            return movimiento;
	        }

	        public int getDanio() {
	            return danio;
	        }

	        public String getTipo() {
	            return tipo;
	        }

	        // Método toString para representación textual del objeto
	        @Override
	        public String toString() {
	            return "Zombie{" +
	                    "movimiento=" + movimiento +
	                    ", danio=" + danio +
	                    ", tipo='" + tipo + '\'' +
	                    '}';
	        }
	    }

	    // Subclase Corredor
	    public class Corredor extends Zombie {
	        // Constructor
	        public Corredor() {
	            super(2, 1, "Corredor");
	        }
	    }

	    // Subclase Caminante
	    public class Caminante extends Zombie {
	        // Constructor
	        public Caminante() {
	            super(1, 1, "Caminante");
	        }
	    }

	    // Subclase Gordo
	    public class Gordo extends Zombie {
	        // Constructor
	        public Gordo() {
	            super(1, 2, "Gordo");
	        }
	    }

	}


